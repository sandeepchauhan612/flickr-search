//
//  FlickrUrlBuilderTests.m
//  Flickr SearchTests
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FlickrURLBuilder.h"
#import "FlickrImageModel.h"
#import "APIConstants.h"

@interface FlickrUrlBuilderTests : XCTestCase

@end

@implementation FlickrUrlBuilderTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testImageUrlBuilder {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(1) forKey:FlickrSearchSearchResultFarm];
    [dict setObject:@"23451156376" forKey:FlickrSearchSearchResultId];
    [dict setObject:@"8983a8ebc7" forKey:FlickrSearchSearchResultSecret];
    [dict setObject:@"578" forKey:FlickrSearchSearchResultServer];
    
    NSString *expectedString = @"http://farm1.static.flickr.com/578/23451156376_8983a8ebc7.jpg";
    FlickrImageModel *imageModel = [[FlickrImageModel alloc] initWithDictionary:[dict copy]];
    NSString *output = [FlickrURLBuilder buildImageUrlFor:imageModel];
    XCTAssertTrue([output isEqualToString:expectedString]);
}

- (void)testSearchUrlBuilder {
    NSString *expectedString = @"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1&safe_search=1&per_page=20&text=kittens&page=1";
    
    NSString *output = [FlickrURLBuilder buildURLForSearch:@"kittens" page:1];
    XCTAssertTrue([output isEqualToString:expectedString]);
}

@end
