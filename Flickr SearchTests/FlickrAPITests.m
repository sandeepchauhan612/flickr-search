//
//  FlickrAPITests.m
//  Flickr SearchTests
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FlickrSearchNetworkHandler.h"
#import "APIConstants.h"

@interface FlickrAPITests : XCTestCase

@end

@implementation FlickrAPITests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAPIbatchSize {
    NSInteger batch = 20;
     XCTestExpectation *expectation = [self expectationWithDescription:[NSString stringWithFormat:@"Count should be %ld", batch]];
    [FlickrSearchNetworkHandler fetchImages:@"kittens" page:1 completion:^(NSArray * _Nullable images, BOOL isNextPageAvaialble) {
        XCTAssertEqual(batch, images.count);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

@end
