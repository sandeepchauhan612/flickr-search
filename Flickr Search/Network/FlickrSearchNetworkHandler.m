//
//  FlickrSearchNetworkHandler.m
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import "FlickrSearchNetworkHandler.h"
#import "APIConstants.h"
#import "FlickrImageModel.h"
#import "FlickrURLBuilder.h"

@implementation FlickrSearchNetworkHandler

+ (void)fetchImages:(NSString *)searchKey page:(NSInteger)page completion:(void (^)(NSArray * _Nullable, BOOL isNextPageAvaialble))completion  {
    NSString *url = [FlickrURLBuilder buildURLForSearch:searchKey page:page];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [urlRequest setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse.statusCode == 200) {
            NSError *error;
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            if (error) {
                if (completion) {
                    completion(nil,NO);
                }
            } else {
                NSDictionary *photosDictionary  = [responseDict objectForKey:FlickerSearchPhotos];
                NSNumber *totalPages = [photosDictionary objectForKey:FlickerSearchTotalPages];
                NSNumber *currentPage = [photosDictionary objectForKey:FlickerSearchCurrentPage];
                BOOL isNextPageAvailable = totalPages != currentPage;
                NSArray *responseArray = [FlickrSearchNetworkHandler parseResponseDict:photosDictionary];
                
                if (completion) {
                    completion(responseArray,isNextPageAvailable);
                }
            }
        }
        else {
            if (completion) {
                completion(nil,NO);
            }
        }
    }];
    [dataTask resume];
}

+ (NSArray *)parseResponseDict:(NSDictionary *)response {
    NSMutableArray *photoModelArray;
    if (response) {
        NSArray *photosArray = [response objectForKey:FlickerSearchPhoto];
        if (photosArray && [photosArray isKindOfClass:[NSArray class]]) {
            photoModelArray = [NSMutableArray array];
            for (NSDictionary *photoDict in photosArray) {
                FlickrImageModel *photo = [[FlickrImageModel alloc] initWithDictionary:photoDict];
                [photoModelArray addObject:photo];
            }
        }
    }
    return [photoModelArray copy];
}

@end
