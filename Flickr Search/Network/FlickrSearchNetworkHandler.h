//
//  FlickrSearchNetworkHandler.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrSearchNetworkHandler : NSObject

+ (void)fetchImages:(NSString *)searchKey page:(NSInteger)page completion:(void (^)(NSArray * _Nullable, BOOL isNextPageAvaialble))completion;

@end
