//
//  FlickrImageCollectionViewCell.m
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import "FlickrImageCollectionViewCell.h"
#import "UIImageView+Cache.h"
#import "FlickrURLBuilder.h"

@interface FlickrImageCollectionViewCell()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, readwrite) NSString *currentURL;

@end

@implementation FlickrImageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor blueColor]];
        [self setUpSubViews];
    }
    return self;
}

- (void)prepareForReuse {
    self.imageView.image = nil;
}

- (void)setUpSubViews {
    self.imageView = [[UIImageView alloc] init];
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.imageView];
    
    [self.imageView.topAnchor constraintEqualToAnchor:self.contentView.topAnchor].active = YES;
    [self.imageView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = YES;
    [self.imageView.leadingAnchor constraintEqualToAnchor:self.contentView.leadingAnchor].active = YES;
    [self.imageView.trailingAnchor constraintEqualToAnchor:self.contentView.trailingAnchor].active = YES;
}

- (void)populateWithModel:(FlickrImageModel *)imageModel {
    self.currentURL = [FlickrURLBuilder buildImageUrlFor:imageModel];
    __weak typeof(self) weakSelf = self;
    [self.imageView setImageWithUrl:self.currentURL uiniqueID:imageModel.imageId completion:^(UIImage * _Nullable image, NSString *downloadedurl) {
        if ([weakSelf.currentURL isEqualToString:downloadedurl]) {
            weakSelf.imageView.image = image;
        }
    }];
}

@end
