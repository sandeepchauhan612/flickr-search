//
//  FlickrImageCollectionViewCell.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrImageModel.h"

@interface FlickrImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, readonly) NSString *currentURL;

- (void)populateWithModel:(FlickrImageModel *)imageModel;

@end
