//
//  FlickrSearchViewController.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrSearchViewModel.h"

@interface FlickrSearchViewController : UIViewController

- (instancetype)initWithViewModel:(FlickrSearchViewModel *)viewModel;

@end
