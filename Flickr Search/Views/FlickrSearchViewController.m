//
//  FlickrSearchViewController.m
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import "FlickrSearchViewController.h"
#import "FlickrStringConstants.h"
#import "FlickrUIConstants.h"
#import "FlickrImageCollectionViewCell.h"
#import "FlickrImageModel.h"

@interface FlickrSearchViewController () <UISearchBarDelegate,UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) FlickrSearchViewModel *viewModel;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation FlickrSearchViewController

- (instancetype)initWithViewModel:(FlickrSearchViewModel *)viewModel {
    if (self = [super init]) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpViews];
}

- (void)fetchDataForSearch:(NSString *)search souldIncreasePage:(BOOL)souldIncreasePage {
    [self.viewModel fetchBatchForSearch:search souldIncreasePage:souldIncreasePage WithCompletion:^(BOOL shouldReload) {
        if(shouldReload) {
            [self.activityIndicator stopAnimating];
            [self.collectionView reloadData];
        } 
    }];
}

// UI SetUp Methods

- (void)setUpViews {
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpSearchBar];
    [self setUpCollectionView];
    [self setUpActivityIndicator];
}

- (void)setUpSearchBar {
    self.searchBar = [[UISearchBar alloc] init];
    self.searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.searchBar];
    [self.searchBar setBarTintColor:[UIColor whiteColor]];
    [self.searchBar.layer setBorderWidth:SearchBarBorderWidth];
    [self.searchBar.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.searchBar.layer setCornerRadius:SearchBarCornerRadius];
    [self.searchBar setClipsToBounds:YES];
    [self.searchBar setPlaceholder:FlickrSearchPlaceholderString];
    self.searchBar.delegate = self;
    [self.searchBar becomeFirstResponder];
    [self.searchBar.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:[[UIApplication sharedApplication] statusBarFrame].size.height].active = YES;
    [self.searchBar.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:SearchBarHorizontalSpacing].active = YES;
    [self.searchBar.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-SearchBarHorizontalSpacing].active = YES;
    [self.searchBar.heightAnchor constraintEqualToConstant:SearchBarHeight].active = YES;
}

- (void)setUpActivityIndicator {
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.activityIndicator];
    
    [self.activityIndicator.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.activityIndicator.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = YES;
}

- (void)setUpCollectionView {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    CGFloat newWidth = ([UIScreen mainScreen].bounds.size.width - ((CollectionViewNumberOfItemsInOneRow-1)* 4.0f));
    CGFloat heightOrWidth = (CGFloat)newWidth/CollectionViewNumberOfItemsInOneRow;
    CGSize itemSize = CGSizeMake(heightOrWidth, heightOrWidth);
    flowLayout.itemSize = itemSize;
    flowLayout.sectionHeadersPinToVisibleBounds = NO;
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.bounces = YES;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    [self.collectionView registerClass:[FlickrImageCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([FlickrImageCollectionViewCell class])];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.collectionView];
    [self.collectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.collectionView.topAnchor constraintEqualToAnchor:self.searchBar.bottomAnchor constant:SearchBarHorizontalSpacing].active = YES;
    [self.collectionView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor].active = YES;
    [self.collectionView.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor].active = YES;
    [self.collectionView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
}

#pragma UICollectionView Delegates

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.viewModel numberOfSections];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.viewModel numberofRowsInSection:section];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FlickrImageCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([FlickrImageCollectionViewCell class]) forIndexPath:indexPath];
    FlickrImageModel *imageModel = [self.viewModel imageItemAtIndex:indexPath.row];
    [cell populateWithModel:imageModel];
    
    if (![self.viewModel isFetching]) {
        if (indexPath.row >= ([self.viewModel numberofRowsInSection:indexPath.section] - 6)) {
            [self fetchDataForSearch:self.viewModel.currentSearchString souldIncreasePage:YES];
        }
    }
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 4.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 4.0f;
}


#pragma SearchBar delegates

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.activityIndicator startAnimating];
    [self fetchDataForSearch:searchBar.text souldIncreasePage:NO];
}

@end
