//
//  APIConstants.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#ifndef APIConstants_h
#define APIConstants_h

// Flick API URL

#define FlickrSearchAPIURL @"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1&safe_search=1&per_page=20"

// Search Result Object Keys
#define FlickrSearchSearchResultId @"id"
#define FlickrSearchSearchResultServer @"server"
#define FlickrSearchSearchResultFarm @"farm"
#define FlickrSearchSearchResultSecret @"secret"

#define FlickerSearchCurrentPage @"page"
#define FlickerSearchTotalPages @"pages"
#define FlickerSearchPhotos @"photos"
#define FlickerSearchPhoto @"photo"

#endif /* APIConstants_h */
