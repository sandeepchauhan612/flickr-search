//
//  FlickrUIConstants.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#ifndef FlickrUIConstants_h
#define FlickrUIConstants_h

// CollectionView Constants

#define CollectionViewNumberOfItemsInOneRow 3.0f

// SearchBar Constants
#define SearchBarCornerRadius 10.0f
#define SearchBarBorderWidth 1.0f
#define SearchBarHeight 40.0f
#define SearchBarHorizontalSpacing 10.0f

#endif /* FlickrUIConstants_h */
