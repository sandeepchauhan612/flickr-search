//
//  UIImageView+Cache.m
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import "UIImageView+Cache.h"

@implementation UIImageView (Cache)

- (void)setImageWithUrl:(NSString *)url uiniqueID:(NSString *)uiniqueID completion:(void (^)(UIImage * _Nullable, NSString * url))completion {
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:uiniqueID];
        NSData *cachedData = [NSData dataWithContentsOfFile:filePath];
        if (!cachedData) {
            cachedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
            if (cachedData) {
                [cachedData writeToFile:filePath atomically:YES];
            }
        } 
        dispatch_async(dispatch_get_main_queue(), ^{
            if (cachedData == nil ) {
                if (completion) {
                    completion(nil,url);
                }
            }
            if (completion) {
                completion([UIImage imageWithData:cachedData],url);
            }
        });
    });
}

@end
