//
//  FlickrURLBuilder.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlickrImageModel.h"

@interface FlickrURLBuilder : NSObject

+ (NSString *)buildURLForSearch:(NSString *)search page:(NSInteger)page;
+ (NSString *)buildImageUrlFor:(FlickrImageModel *)model;

@end
