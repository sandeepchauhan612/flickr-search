//
//  UIImageView+Cache.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Cache)

- (void)setImageWithUrl:(NSString *)url uiniqueID:(NSString *)uiniqueID completion:(void (^)(UIImage * _Nullable, NSString * url))completion;

@end
