//
//  FlickrStringConstants.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#ifndef FlickrStringConstants_h
#define FlickrStringConstants_h

#define FlickrSearchPlaceholderString @"Search"

#endif /* FlickrStringConstants_h */
