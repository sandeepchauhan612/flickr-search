//
//  FlickrURLBuilder.m
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import "FlickrURLBuilder.h"
#import "APIConstants.h"

@implementation FlickrURLBuilder

+ (NSString *)buildURLForSearch:(NSString *)search page:(NSInteger)page {
    return [NSString stringWithFormat:@"%@&text=%@&page=%ld",FlickrSearchAPIURL,search,page];
}

+ (NSString *)buildImageUrlFor:(FlickrImageModel *)model {
    NSString *url = [NSString stringWithFormat:@"http://farm%ld.static.flickr.com/%@/%@_%@.jpg",model.imageFarm.integerValue,model.imageServer,model.imageId,model.imageSecret];
    return url;
}

@end
