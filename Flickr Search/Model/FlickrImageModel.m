//
//  FlickrImageModel.m
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import "FlickrImageModel.h"
#import "APIConstants.h"

@interface FlickrImageModel()

@property (nonatomic, readwrite) NSString *imageId;
@property (nonatomic, readwrite) NSString *imageSecret;
@property (nonatomic, readwrite) NSString *imageServer;
@property (nonatomic, readwrite) NSNumber *imageFarm;

@end

@implementation FlickrImageModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if(self == [super init]) {
        _imageId  = [dictionary objectForKey:FlickrSearchSearchResultId];
        _imageSecret = [dictionary objectForKey:FlickrSearchSearchResultSecret];
        _imageServer = [dictionary objectForKey:FlickrSearchSearchResultServer];
        _imageFarm = [dictionary objectForKey:FlickrSearchSearchResultFarm];
    }
    return self;
}

@end
