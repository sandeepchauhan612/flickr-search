//
//  FlickrImageModel.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickrImageModel : NSObject

@property (nonatomic, readonly) NSString *imageId;
@property (nonatomic, readonly) NSString *imageSecret;
@property (nonatomic, readonly) NSString *imageServer;
@property (nonatomic, readonly) NSNumber *imageFarm;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
