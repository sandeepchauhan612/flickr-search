//
//  FlickrSearchViewModel.m
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import "FlickrSearchViewModel.h"
#import "FlickrSearchNetworkHandler.h"

@interface FlickrSearchViewModel()

@property (nonatomic, strong) NSMutableArray<FlickrImageModel *> *imagesArray;
@property (nonatomic, strong) NSString *currentSearchString;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL isNextPageAvailable;
@property (nonatomic, assign) BOOL isFetchingData;

@end

@implementation FlickrSearchViewModel

- (instancetype)init {
    if (self = [super init]) {
        _imagesArray = [NSMutableArray array];
    }
    return self;
}

- (NSInteger)numberOfSections {
    return 1;
}

- (NSInteger)numberofRowsInSection:(NSInteger)section {
    return self.imagesArray.count;
}

- (FlickrImageModel *)imageItemAtIndex:(NSInteger)index {
    return [self.imagesArray objectAtIndex:index];
}

- (BOOL)isFetching {
    return self.isFetchingData;
}

- (void)fetchBatchForSearch:(NSString *)string souldIncreasePage:(BOOL)souldIncreasePage WithCompletion:(void (^)(BOOL shouldReload))completion {
    if (![self.currentSearchString isEqualToString:string]) {
        self.currentSearchString = string;
        self.currentPage = 1;
        self.isNextPageAvailable = YES;
    } else {
        if (souldIncreasePage){
            self.currentPage++;
        } else {
            if(completion){
                completion(NO);
            }
            return;
        }
    }
    
    self.isFetchingData = YES;
    [FlickrSearchNetworkHandler fetchImages:self.currentSearchString page:self.currentPage completion:^(NSArray * _Nullable images, BOOL isNextPageAvaialble) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.isFetchingData = NO;
            if (images.count) {
                self.isNextPageAvailable = isNextPageAvaialble;
                [self.imagesArray addObjectsFromArray:images];
                if (completion) {
                    completion(YES);
                }
            } else {
                if (completion) {
                    completion(NO);
                }
            }
        });
    }];
}

@end
