//
//  FlickrSearchViewModel.h
//  Flickr Search
//
//  Created by Sandeep Chauhan on 22/06/18.
//  Copyright © 2018 flickrSearch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FlickrImageModel.h"

@interface FlickrSearchViewModel : NSObject

@property (nonatomic, readonly) NSString *currentSearchString;

- (NSInteger)numberOfSections;
- (NSInteger)numberofRowsInSection:(NSInteger)section;
- (FlickrImageModel *)imageItemAtIndex:(NSInteger)index;
- (BOOL)isFetching;
- (void)fetchBatchForSearch:(NSString *)string souldIncreasePage:(BOOL)souldIncreasePage WithCompletion:(void (^)(BOOL shouldReload))completion;

@end
